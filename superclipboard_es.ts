<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>SuperClipboard</name>
    <message>
        <location filename="SuperClipboard.ui" line="14"/>
        <source>SuperClipboard</source>
        <translation>SuperClipboard</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="62"/>
        <source>Slots</source>
        <translation>Ranuras</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="75"/>
        <location filename="SuperClipboard.ui" line="183"/>
        <location filename="SuperClipboard.ui" line="287"/>
        <source>Slot</source>
        <translation>Ranura</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="80"/>
        <location filename="SuperClipboard.ui" line="188"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="83"/>
        <location filename="SuperClipboard.ui" line="191"/>
        <source>Date and time when the slot was stored</source>
        <translation>Fecha y hora de la ranura</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="88"/>
        <location filename="SuperClipboard.ui" line="196"/>
        <source>Tag</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="93"/>
        <location filename="SuperClipboard.ui" line="201"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="96"/>
        <location filename="SuperClipboard.ui" line="204"/>
        <source>Size of the total data in bytes</source>
        <translation>Tamaño total en bytes</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="101"/>
        <location filename="SuperClipboard.ui" line="209"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="104"/>
        <location filename="SuperClipboard.ui" line="212"/>
        <source>Plain text from the clipboard that may be cut off</source>
        <translation>Texto plano (puede estar cortado)</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="109"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="114"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="119"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="124"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="129"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="134"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="139"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="144"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="149"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="179"/>
        <source>History</source>
        <translation>Historial</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="231"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="235"/>
        <source>Store clipboard into...</source>
        <translation>Almacenar en...</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="253"/>
        <source>Retrieve clipboard from...</source>
        <translation>Recuperar desde...</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="276"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="306"/>
        <source>Quit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="311"/>
        <location filename="SuperClipboard.ui" line="356"/>
        <source>Slot 1</source>
        <translation>Ranura 1</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="316"/>
        <location filename="SuperClipboard.ui" line="361"/>
        <source>Slot 2</source>
        <translation>Ranura 2</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="321"/>
        <location filename="SuperClipboard.ui" line="366"/>
        <source>Slot 3</source>
        <translation>Ranura 3</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="326"/>
        <location filename="SuperClipboard.ui" line="371"/>
        <source>Slot 4</source>
        <translation>Ranura 4</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="331"/>
        <location filename="SuperClipboard.ui" line="376"/>
        <source>Slot 5</source>
        <translation>Ranura 5</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="336"/>
        <location filename="SuperClipboard.ui" line="381"/>
        <source>Slot 6</source>
        <translation>Ranura 6</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="341"/>
        <location filename="SuperClipboard.ui" line="386"/>
        <source>Slot 7</source>
        <translation>Ranura 7</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="346"/>
        <location filename="SuperClipboard.ui" line="391"/>
        <source>Slot 8</source>
        <translation>Ranura 8</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="351"/>
        <location filename="SuperClipboard.ui" line="396"/>
        <source>Slot 9</source>
        <translation>Ranura 9</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="405"/>
        <location filename="SuperClipboard.cpp" line="150"/>
        <location filename="SuperClipboard.cpp" line="171"/>
        <source>Shortcuts</source>
        <translation>Atajos de teclado</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="414"/>
        <location filename="SuperClipboard.cpp" line="141"/>
        <source>About SuperClipboard</source>
        <translation>Acerca de SuperClipboard</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="423"/>
        <source>Retrieve from selected slot</source>
        <translation>Recuperar ranura seleccionada</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="432"/>
        <source>Open selected slot with text editor</source>
        <translation>Ver con editor de texto</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="441"/>
        <source>Follow link in default browser</source>
        <translation>Abrir enlace en el navegador</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="450"/>
        <source>Clear selected slot</source>
        <translation>Limpiar ranura seleccionada</translation>
    </message>
    <message>
        <location filename="SuperClipboard.ui" line="459"/>
        <source>Store in selected slot</source>
        <translation>Almacenar en la ranura seleccionada</translation>
    </message>
    <message>
        <location filename="SuperClipboard.cpp" line="137"/>
        <source>SuperClipboard is written in C++ using Qt libraries. It enables users to save the clipboard state into &quot;slots&quot;, which are actually files. Then the state of the clipboard can be recovered.</source>
        <translation>SuperClipboard está escrito en C++, usando las librerías de Qt. Permite almacenar el estado del portapapeles en ficheros llamados &quot;ranuras&quot;, para posteriormente recuperar dicho estado.</translation>
    </message>
    <message>
        <location filename="SuperClipboard.cpp" line="139"/>
        <source>In Unix systems SuperClipboard requires Xlib to detect key combinations. In Windows systems Win API is used instead.</source>
        <translation>En sistemas Unix, SuperClipboard requiere Xlib para detectar combinaciones de teclas. En sistemas Windows, se utiliza Win API para detectar combinaciones de teclas.</translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="151"/>
        <location filename="SuperClipboard.cpp" line="152"/>
        <location filename="SuperClipboard.cpp" line="153"/>
        <location filename="SuperClipboard.cpp" line="154"/>
        <location filename="SuperClipboard.cpp" line="155"/>
        <location filename="SuperClipboard.cpp" line="156"/>
        <location filename="SuperClipboard.cpp" line="157"/>
        <location filename="SuperClipboard.cpp" line="158"/>
        <location filename="SuperClipboard.cpp" line="159"/>
        <location filename="SuperClipboard.cpp" line="411"/>
        <source>Store into slot %n</source>
        <translation>
            <numerusform>Almacenar en ranura %n</numerusform>
            <numerusform>Almacenar en ranura %n</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="160"/>
        <location filename="SuperClipboard.cpp" line="161"/>
        <location filename="SuperClipboard.cpp" line="162"/>
        <location filename="SuperClipboard.cpp" line="163"/>
        <location filename="SuperClipboard.cpp" line="164"/>
        <location filename="SuperClipboard.cpp" line="165"/>
        <location filename="SuperClipboard.cpp" line="166"/>
        <location filename="SuperClipboard.cpp" line="167"/>
        <location filename="SuperClipboard.cpp" line="168"/>
        <location filename="SuperClipboard.cpp" line="412"/>
        <source>Retrieve from slot %n</source>
        <translation>
            <numerusform>Recuperar desde la ranura %n</numerusform>
            <numerusform>Recuperar desde la ranura %n</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="221"/>
        <source>Stored into %n</source>
        <translation>
            <numerusform>Almacenado en %n</numerusform>
            <numerusform>Almacenado en %n</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="224"/>
        <source>Failed to store into %n</source>
        <translation>
            <numerusform>Falló al almacenar en %n</numerusform>
            <numerusform>Falló al almacenar en %n</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="232"/>
        <source>Retrieved from %n</source>
        <translation>
            <numerusform>Recuperado desde %n</numerusform>
            <numerusform>Recuperado desde %n</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="235"/>
        <source>Failed to retrieve from %n</source>
        <translation>
            <numerusform>Falló al recuperar desde %n</numerusform>
            <numerusform>Falló al recuperar desde %n</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="285"/>
        <source>Retrieved from history %n</source>
        <translation>
            <numerusform>Recuperado desde el historial %n</numerusform>
            <numerusform>Recuperado desde el historial %n</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="288"/>
        <source>Failed to retrieve from history %n</source>
        <translation>
            <numerusform>Falló al recuperar desde el historial %n</numerusform>
            <numerusform>Falló al recuperar desde el historial %n</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="340"/>
        <source>Failed to store history %n</source>
        <translation>
            <numerusform>Falló al almacenar en el historial %n</numerusform>
            <numerusform>Falló al almacenar en el historial %n</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="413"/>
        <source>Open slot %n with text editor</source>
        <translation>
            <numerusform>Ver slot %n en editor de texto</numerusform>
            <numerusform>Ver slot %n en editor de texto</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="414"/>
        <source>Follow link slot %n in broswer</source>
        <translation>
            <numerusform>Abrir enlace en el navegador</numerusform>
            <numerusform>Abrir enlace en el navegador</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="415"/>
        <source>Clear slot %n</source>
        <translation>
            <numerusform>Limpiar ranura %n</numerusform>
            <numerusform>Limpiar ranura %n</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="456"/>
        <source>Store into history %n</source>
        <translation>
            <numerusform>Almacenar en el historial %n</numerusform>
            <numerusform>Almacenar en el historial %n</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="457"/>
        <source>Retrieve from history %n</source>
        <translation>
            <numerusform>Recuperar desde el historial %n</numerusform>
            <numerusform>Recuperar desde el historial %n</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="458"/>
        <source>Open history %n with text editor</source>
        <translation>
            <numerusform>Ver historial %n en editor de texto</numerusform>
            <numerusform>Ver historial %n en editor de texto</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="459"/>
        <source>Follow link history %n in broswer</source>
        <translation>
            <numerusform>Abrir enlace en el navegador</numerusform>
            <numerusform>Abrir enlace en el navegador</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="SuperClipboard.cpp" line="460"/>
        <source>Clear history %n</source>
        <translation>
            <numerusform>Limpiar historial %n</numerusform>
            <numerusform>Limpiar historial %n</numerusform>
        </translation>
    </message>
</context>
</TS>
