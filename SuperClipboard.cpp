#include "SuperClipboard.h"
#include "ui_SuperClipboard.h"

#include <QMessageBox>
#include <QApplication>
#include <QClipboard>
#include <QDateTime>
#include <QHostInfo>
#include <QFile>
#include <QDir>
#include <QTemporaryFile>
#include <QDesktopServices>
#include <QUrl>

#ifdef WIN32
#include "SystemKeyboardWin.h"
#else
#include "SystemKeyboardX11.h"
#endif

SuperClipboard::SuperClipboard(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SuperClipboard),
    systemTrayIcon(QIcon(":/resources/clipboard.png"), this),
    watcher(SLOTS_PATH, this),
    watcherHistory(HISTORY_PATH, this)
{
    ui->setupUi(this);

    systemTrayIcon.show();
    systemTrayIcon.setContextMenu(ui->menuFile);
    systemTrayIcon.setToolTip("SuperClipboard " + VERSION);
    connect(&systemTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(systemTrayIcon_activated(QSystemTrayIcon::ActivationReason)));

#if QT_VERSION < 0x050000
    connect(&watcher, SIGNAL(fileRemoved(QString)), this, SLOT(watcher_slotTouched(QString)));
    connect(&watcher, SIGNAL(fileChanged(QString)), this, SLOT(watcher_slotTouched(QString)));
    connect(&watcher, SIGNAL(fileAdded(QString)), this, SLOT(watcher_slotTouched(QString)));
    connect(&watcherHistory, SIGNAL(fileRemoved(QString)), this, SLOT(watcherHistory_slotTouched(QString)));
    connect(&watcherHistory, SIGNAL(fileChanged(QString)), this, SLOT(watcherHistory_slotTouched(QString)));
    connect(&watcherHistory, SIGNAL(fileAdded(QString)), this, SLOT(watcherHistory_slotTouched(QString)));
#else
    connect(&watcher, &DirectoryWatcher::fileRemoved, this, &SuperClipboard::watcher_slotTouched);
    connect(&watcher, &DirectoryWatcher::fileChanged, this, &SuperClipboard::watcher_slotTouched);
    connect(&watcher, &DirectoryWatcher::fileAdded, this, &SuperClipboard::watcher_slotTouched);
    connect(&watcherHistory, &DirectoryWatcher::fileRemoved, this, &SuperClipboard::watcherHistory_slotTouched);
    connect(&watcherHistory, &DirectoryWatcher::fileChanged, this, &SuperClipboard::watcherHistory_slotTouched);
    connect(&watcherHistory, &DirectoryWatcher::fileAdded, this, &SuperClipboard::watcherHistory_slotTouched);
#endif

    for (int i = 1; i <= 9; i++) {
        refreshSlot(i);
    }

    QStringList entryList = QDir(HISTORY_PATH).entryList(QStringList("*.metadata"), QDir::NoFilter, QDir::Name);
    foreach (const QString &entry, entryList) {
        refreshSlotHistory(entry.left(18));
    }

#ifdef WIN32
    SystemKeyboardWin::instance()->setSuperClipboard(this);
#else
    systemKeyboardX11 = new SystemKeyboardX11(this);
    connect(systemKeyboardX11, SIGNAL(keyPressed(int,bool)), this, SLOT(keyPressed(int, bool)));
    systemKeyboardX11->start();
#endif

    ui->tableWidgetHistory->verticalHeader()->setVisible(false);
    ui->tableWidgetSlots->verticalHeader()->setVisible(false);
}

SuperClipboard::~SuperClipboard()
{
#ifdef WIN32
    delete SystemKeyboardWin::instance();
#else
    systemKeyboardX11->quit();
    delete systemKeyboardX11;
#endif

    delete ui;
}

#ifndef WIN32
void SuperClipboard::keyPressed(int index, bool store)
{
    if (store) {
        storeClipboard(index);
    } else {
        retrieveClipboard(index);
    }
}
#endif

void SuperClipboard::closeEvent(QCloseEvent *event)
{
    hide();
    event->ignore();
}

void SuperClipboard::systemTrayIcon_activated(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::MiddleClick || reason == QSystemTrayIcon::Trigger) {
        if (isVisible()) {
            hide();
        } else {
            showMaximized();
        }
    }
}

void SuperClipboard::watcher_slotTouched(const QString &path) {
    if (path.endsWith(".metadata")) {
        int index = path.split(".metadata").first().right(1).toInt();
        refreshSlot(index);
    }
}

void SuperClipboard::watcherHistory_slotTouched(const QString &path) {
    if (path.endsWith(".metadata")) {
        QFileInfo fileInfo(path);
        refreshSlotHistory(fileInfo.baseName().split(".metadata").first());
    }
}


void SuperClipboard::on_actionQuit_triggered()
{
    QApplication::quit();
}

void SuperClipboard::on_actionAbout_SuperClipboard_triggered()
{
    QString message;
    message = "<h1>SuperClipboard " + VERSION + "</h1>";
    message = message + "<small>Christian Aguilera, " + __DATE__ + " " + __TIME__ + "</small><br><br><br>";
    message = message + tr("SuperClipboard is written in C++ using Qt libraries. It enables users to save the clipboard state into \"slots\", which are actually files. Then the state of the clipboard can be recovered.");
    message = message + "<br><br>";
    message = message + tr("In Unix systems SuperClipboard requires Xlib to detect key combinations. In Windows systems Win API is used instead.");

    QMessageBox messageBox(QMessageBox::NoIcon, tr("About SuperClipboard"), message, QMessageBox::Close, this);
    QIcon icon = QIcon(":/resources/clipboard.png");
    messageBox.setIconPixmap(icon.pixmap(icon.actualSize(QSize(128, 128))));
    messageBox.exec();
}

void SuperClipboard::on_actionShortcuts_triggered()
{
    QString message;
    message = "<h1>" + tr("Shortcuts") + "</h1>";
    message = message + tr("Store into slot %n", "", 1) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+1</strong><br>";
    message = message + tr("Store into slot %n", "", 2) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+2</strong><br>";
    message = message + tr("Store into slot %n", "", 3) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+3</strong><br>";
    message = message + tr("Store into slot %n", "", 4) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+4</strong><br>";
    message = message + tr("Store into slot %n", "", 5) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+5</strong><br>";
    message = message + tr("Store into slot %n", "", 6) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+6</strong><br>";
    message = message + tr("Store into slot %n", "", 7) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+7</strong><br>";
    message = message + tr("Store into slot %n", "", 8) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+8</strong><br>";
    message = message + tr("Store into slot %n", "", 9) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+9</strong><br><br>";
    message = message + tr("Retrieve from slot %n", "", 1) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+F1</strong><br>";
    message = message + tr("Retrieve from slot %n", "", 2) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+F2</strong><br>";
    message = message + tr("Retrieve from slot %n", "", 3) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+F3</strong><br>";
    message = message + tr("Retrieve from slot %n", "", 4) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+F4</strong><br>";
    message = message + tr("Retrieve from slot %n", "", 5) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+F5</strong><br>";
    message = message + tr("Retrieve from slot %n", "", 6) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+F6</strong><br>";
    message = message + tr("Retrieve from slot %n", "", 7) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+F7</strong><br>";
    message = message + tr("Retrieve from slot %n", "", 8) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+F8</strong><br>";
    message = message + tr("Retrieve from slot %n", "", 9) + ": &nbsp;&nbsp;<strong>Ctrl+Shift+F9</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>";
    message = message + "<br><br>";

    QMessageBox messageBox(QMessageBox::NoIcon, tr("Shortcuts"), message, QMessageBox::Close, this);
    QIcon icon = QIcon(":/resources/shortcuts.png");
    messageBox.setIconPixmap(icon.pixmap(icon.actualSize(QSize(64, 64))));
    messageBox.exec();
}

void SuperClipboard::refreshSlot(int index)
{
    QString text;
    QDateTime dateTime;
    QString tag;
    int size = 0;
    if (clipboardManager.read(SLOTS_PATH + "slot" + QString::number(index) + ".metadata", text, dateTime, tag)) {
        size = QFile(SLOTS_PATH + "slot" + QString::number(index) + ".mimedata").size();
        ui->tableWidgetSlots->setItem(index - 1, 0, new QTableWidgetItem(QString::number(index)));
        ui->tableWidgetSlots->setItem(index - 1, 1, new QTableWidgetItem(dateTime.toString("yyyy-MM-dd hh:mm:ss.zzz")));
        ui->tableWidgetSlots->setItem(index - 1, 2, new QTableWidgetItem(tag));
        ui->tableWidgetSlots->setItem(index - 1, 3, new QTableWidgetItem(QString::number(size)));
        ui->tableWidgetSlots->setItem(index - 1, 4, new QTableWidgetItem(text));
    } else {
        ui->tableWidgetSlots->setItem(index - 1, 0, new QTableWidgetItem(QString::number(index)));
        ui->tableWidgetSlots->setItem(index - 1, 1, new QTableWidgetItem(""));
        ui->tableWidgetSlots->setItem(index - 1, 2, new QTableWidgetItem(""));
        ui->tableWidgetSlots->setItem(index - 1, 3, new QTableWidgetItem(""));
        ui->tableWidgetSlots->setItem(index - 1, 4, new QTableWidgetItem(""));
    }

    ui->tableWidgetSlots->item(index - 1, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->tableWidgetSlots->item(index - 1, 1)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->tableWidgetSlots->item(index - 1, 2)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->tableWidgetSlots->item(index - 1, 3)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    ui->tableWidgetSlots->resizeColumnsToContents();
#if QT_VERSION < 0x050000
    ui->tableWidgetSlots->verticalHeader()->setResizeMode(QHeaderView::Stretch);
    ui->tableWidgetSlots->horizontalHeader()->setResizeMode(ui->tableWidgetSlots->columnCount() - 1, QHeaderView::Stretch);
#else
    ui->tableWidgetSlots->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidgetSlots->horizontalHeader()->setSectionResizeMode(ui->tableWidgetSlots->columnCount() - 1, QHeaderView::Stretch);
#endif
}

void SuperClipboard::storeClipboard(int index) {
    bool result = true;
    const QMimeData *mimeData = QApplication::clipboard()->mimeData();
    result &= clipboardManager.write(SLOTS_PATH + "slot" + QString::number(index) + ".mimedata", *mimeData);
    result &= clipboardManager.write(SLOTS_PATH + "slot" + QString::number(index) + ".metadata", mimeData->text().mid(0, 1024), QDateTime::currentDateTime(), QHostInfo::localHostName());

    if (result) {
        refreshSlot(index);
        systemTrayIcon.showMessage(tr("Stored into %n", "", index), "", QSystemTrayIcon::Information, 2000);
        storeClipboardHistory(index);
    } else {
        systemTrayIcon.showMessage(tr("Failed to store into %n", "", index), "", QSystemTrayIcon::Critical, 2000);
    }
}

void SuperClipboard::retrieveClipboard(int index) {
    QMimeData *mimeData = new QMimeData();
    if (clipboardManager.read(SLOTS_PATH + "slot" + QString::number(index) + ".mimedata", *mimeData)) {
        QApplication::clipboard()->setMimeData(mimeData);
        systemTrayIcon.showMessage(tr("Retrieved from %n", "", index), "", QSystemTrayIcon::Information, 2000);
    } else {
        delete mimeData;
        systemTrayIcon.showMessage(tr("Failed to retrieve from %n", "", index), "", QSystemTrayIcon::Critical, 2000);
    }
}

void SuperClipboard::clearClipboard(int index) {
    QFile::remove(SLOTS_PATH + "slot" + QString::number(index) + ".mimedata");
    QFile::remove(SLOTS_PATH + "slot" + QString::number(index) + ".metadata");
    refreshSlot(index);
}

void SuperClipboard::openClipboard(int index) {
    QMimeData *mimeData = new QMimeData();
    if (clipboardManager.read(SLOTS_PATH + "slot" + QString::number(index) + ".mimedata", *mimeData)) {
        QString fileName = QDir::tempPath() + "/" + "superclipboard_slot" + QString::number(index) + "_XXXXXX.txt";
        QTemporaryFile file(fileName);
        file.setAutoRemove(false);
        if (file.open()) {
            foreach (const QString &str, mimeData->formats()) {
                file.write(str.toStdString().c_str());
#ifndef WIN32
                file.write("\n");
#else
                file.write("\r\n");
#endif
            }
            file.write(mimeData->text().toStdString().c_str());
            file.close();

#ifndef WIN32
            QDesktopServices::openUrl(QUrl("file://" + file.fileName()));
#else
            QDesktopServices::openUrl(QUrl(file.fileName()));
#endif
        }
    }
    delete mimeData;
}

void SuperClipboard::followClipboard(int index) {
    QMimeData *mimeData = new QMimeData();
    if (clipboardManager.read(SLOTS_PATH + "slot" + QString::number(index) + ".mimedata", *mimeData)) {
        QDesktopServices::openUrl(QUrl(mimeData->text().trimmed()));
    }
    delete mimeData;
}

void SuperClipboard::retrieveClipboardHistory(const QString &label) {
    QMimeData *mimeData = new QMimeData();
    if (clipboardManager.read(HISTORY_PATH + label + ".mimedata", *mimeData)) {
        QApplication::clipboard()->setMimeData(mimeData);
        systemTrayIcon.showMessage(tr("Retrieved from history %n", "", QString(label[17]).toInt()), "", QSystemTrayIcon::Information, 2000);
    } else {
        delete mimeData;
        systemTrayIcon.showMessage(tr("Failed to retrieve from history %n", "", QString(label[17]).toInt()), "", QSystemTrayIcon::Critical, 2000);
    }
}

void SuperClipboard::refreshSlotHistory(const QString &label)
{
    QString slot = QString(label[17]);
    QString text;
    QString dateTimeFromLabel = label;
    dateTimeFromLabel.chop(1);
    QDateTime dateTime = QDateTime::fromString(dateTimeFromLabel, "yyyyMMddhhmmsszzz");
    QString tag;
    int size = 0;
    if (clipboardManager.read(HISTORY_PATH + label + ".metadata", text, dateTime, tag)) {
        size = QFile(HISTORY_PATH + label + ".mimedata").size();
        ui->tableWidgetHistory->insertRow(0);
        ui->tableWidgetHistory->setItem(0, 0, new QTableWidgetItem(slot));
        ui->tableWidgetHistory->setItem(0, 1, new QTableWidgetItem(QDateTime::fromString(dateTimeFromLabel, "yyyyMMddhhmmsszzz").toString("yyyy-MM-dd hh:mm:ss.zzz")));
        ui->tableWidgetHistory->setItem(0, 2, new QTableWidgetItem(tag));
        ui->tableWidgetHistory->setItem(0, 3, new QTableWidgetItem(QString::number(size)));
        ui->tableWidgetHistory->setItem(0, 4, new QTableWidgetItem(text));

        ui->tableWidgetHistory->item(0, 0)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        ui->tableWidgetHistory->item(0, 1)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        ui->tableWidgetHistory->item(0, 2)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        ui->tableWidgetHistory->item(0, 3)->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    } else {
        QString dateTimeString = dateTime.toString("yyyy-MM-dd hh:mm:ss.zzz");
        for (int i = 0; i < ui->tableWidgetHistory->rowCount(); i++) {
            if (ui->tableWidgetHistory->item(i, 0)->text() == slot && ui->tableWidgetHistory->item(i, 1)->text() == dateTimeString) {
                ui->tableWidgetHistory->removeRow(i);
                break;
            }
        }
    }

    ui->tableWidgetHistory->resizeColumnsToContents();
#if QT_VERSION < 0x050000
    ui->tableWidgetHistory->horizontalHeader()->setResizeMode(ui->tableWidgetHistory->columnCount() - 1, QHeaderView::Stretch);
#else
    ui->tableWidgetHistory->horizontalHeader()->setSectionResizeMode(ui->tableWidgetHistory->columnCount() - 1, QHeaderView::Stretch);
#endif
}

void SuperClipboard::storeClipboardHistory(int index) {
    bool result = true;
    const QMimeData *mimeData = QApplication::clipboard()->mimeData();
    const QDateTime &dateTime = QDateTime::currentDateTime();
    result &= clipboardManager.write(HISTORY_PATH + dateTime.toString("yyyyMMddhhmmsszzz") + QString::number(index) + ".mimedata", *mimeData);
    result &= clipboardManager.write(HISTORY_PATH + dateTime.toString("yyyyMMddhhmmsszzz") + QString::number(index) + ".metadata", mimeData->text().mid(0, 1024), dateTime, QHostInfo::localHostName());

    if (!result) {
        systemTrayIcon.showMessage(tr("Failed to store history %n", "", index), "", QSystemTrayIcon::Critical, 2000);
    }
}

void SuperClipboard::clearClipboardHistory(const QString &label) {
    QFile::remove(HISTORY_PATH + label + ".mimedata");
    QFile::remove(HISTORY_PATH + label + ".metadata");
    refreshSlotHistory(label);
}

void SuperClipboard::followClipboardHistory(const QString &label) {
    QMimeData *mimeData = new QMimeData();
    if (clipboardManager.read(HISTORY_PATH + label + ".mimedata", *mimeData)) {
        QDesktopServices::openUrl(QUrl(mimeData->text().trimmed()));
    }
    delete mimeData;
}

void SuperClipboard::openClipboardHistory(const QString &label) {
    QMimeData *mimeData = new QMimeData();
    if (clipboardManager.read(HISTORY_PATH + label + ".mimedata", *mimeData)) {
        QString fileName = QDir::tempPath() + "/" + "superclipboard_history" + label + "_XXXXXX.txt";
        QTemporaryFile file(fileName);
        file.setAutoRemove(false);
        if (file.open()) {
            foreach (const QString &str, mimeData->formats()) {
                file.write(str.toStdString().c_str());
#ifndef WIN32
                file.write("\n");
#else
                file.write("\r\n");
#endif
            }
            file.write(mimeData->text().toStdString().c_str());
            file.close();

#ifndef WIN32
            QDesktopServices::openUrl(QUrl("file://" + file.fileName()));
#else
            QDesktopServices::openUrl(QUrl(file.fileName()));
#endif
        }
    }
    delete mimeData;
}

void SuperClipboard::on_tableWidgetSlots_cellDoubleClicked(int row, int)
{
    retrieveClipboard(row + 1);
}

void SuperClipboard::on_tableWidgetSlots_cellChanged(int, int column)
{
    if (column == 0) {
        on_tableWidgetSlots_itemSelectionChanged();
    }
}

void SuperClipboard::on_tableWidgetSlots_itemSelectionChanged()
{
    QList<QTableWidgetItem*> list = ui->tableWidgetSlots->selectedItems();
    if (!list.isEmpty()) {
        int row = ui->tableWidgetSlots->row(list.first());
        ui->menuSlot->setEnabled(true);
        bool exists = QFile::exists(SLOTS_PATH + "slot" + QString::number(row + 1) + ".mimedata");
        exists = exists ? true : QFile::exists(SLOTS_PATH + "slot" + QString::number(row + 1) + ".metadata");
        ui->actionStoreSelectedSlot->setVisible(true);
        ui->actionRetrieveSelectedSlot->setEnabled(exists);
        ui->actionOpenSelectedSlot->setEnabled(exists);
        ui->actionFollowSelectedSlot->setEnabled(exists);
        ui->actionClearSelectedSlot->setEnabled(exists);
        ui->actionStoreSelectedSlot->setText(tr("Store into slot %n", "", row + 1));
        ui->actionRetrieveSelectedSlot->setText(tr("Retrieve from slot %n", "", row + 1));
        ui->actionOpenSelectedSlot->setText(tr("Open slot %n with text editor", "", row + 1));
        ui->actionFollowSelectedSlot->setText(tr("Follow link slot %n in broswer", "", row + 1));
        ui->actionClearSelectedSlot->setText(tr("Clear slot %n", "", row + 1));
    } else {
        ui->menuSlot->setEnabled(false);
    }
}

void SuperClipboard::on_tableWidgetSlots_customContextMenuRequested(const QPoint &pos)
{
    on_tableWidgetSlots_itemSelectionChanged();
    ui->menuSlot->exec(ui->tableWidgetSlots->viewport()->mapToGlobal(pos));
}

void SuperClipboard::on_tableWidgetHistory_cellDoubleClicked(int row, int)
{
    QString label = ui->tableWidgetHistory->item(row, 1)->text() + ui->tableWidgetHistory->item(row, 0)->text();
    label.replace("-", "").replace(" ", "").replace(":", "").replace(".", "");
    retrieveClipboardHistory(label);
}

void SuperClipboard::on_tableWidgetHistory_cellChanged(int, int column)
{
    if (column == 0) {
        on_tableWidgetHistory_itemSelectionChanged();
    }
}

void SuperClipboard::on_tableWidgetHistory_itemSelectionChanged()
{
    QList<QTableWidgetItem*> list = ui->tableWidgetHistory->selectedItems();
    if (!list.isEmpty()) {
        int row = ui->tableWidgetHistory->row(list.first());
        QString label = ui->tableWidgetHistory->item(row, 1)->text() + ui->tableWidgetHistory->item(row, 0)->text();
        label.replace("-", "").replace(" ", "").replace(":", "").replace(".", "");
        ui->menuSlot->setEnabled(true);
        bool exists = QFile::exists(HISTORY_PATH + label + ".mimedata");
        exists = exists ? true : QFile::exists(HISTORY_PATH + label + ".metadata");
        ui->actionStoreSelectedSlot->setVisible(false);
        ui->actionRetrieveSelectedSlot->setEnabled(exists);
        ui->actionOpenSelectedSlot->setEnabled(exists);
        ui->actionFollowSelectedSlot->setEnabled(exists);
        ui->actionClearSelectedSlot->setEnabled(exists);
        ui->actionStoreSelectedSlot->setText(tr("Store into history %n", "", QString(label[17]).toInt()));
        ui->actionRetrieveSelectedSlot->setText(tr("Retrieve from history %n", "", QString(label[17]).toInt()));
        ui->actionOpenSelectedSlot->setText(tr("Open history %n with text editor", "", QString(label[17]).toInt()));
        ui->actionFollowSelectedSlot->setText(tr("Follow link history %n in broswer", "", QString(label[17]).toInt()));
        ui->actionClearSelectedSlot->setText(tr("Clear history %n", "", QString(label[17]).toInt()));
    } else {
        ui->menuSlot->setEnabled(false);
    }
}

void SuperClipboard::on_tableWidgetHistory_customContextMenuRequested(const QPoint &pos)
{
    on_tableWidgetHistory_itemSelectionChanged();
    ui->menuSlot->exec(ui->tableWidgetHistory->viewport()->mapToGlobal(pos));
}

void SuperClipboard::on_actionStoreSlot_1_triggered()
{
    storeClipboard(1);
}

void SuperClipboard::on_actionStoreSlot_2_triggered()
{
    storeClipboard(2);
}

void SuperClipboard::on_actionStoreSlot_3_triggered()
{
    storeClipboard(3);
}

void SuperClipboard::on_actionStoreSlot_4_triggered()
{
    storeClipboard(4);
}

void SuperClipboard::on_actionStoreSlot_5_triggered()
{
    storeClipboard(5);
}

void SuperClipboard::on_actionStoreSlot_6_triggered()
{
    storeClipboard(6);
}

void SuperClipboard::on_actionStoreSlot_7_triggered()
{
    storeClipboard(7);
}

void SuperClipboard::on_actionStoreSlot_8_triggered()
{
    storeClipboard(8);
}

void SuperClipboard::on_actionStoreSlot_9_triggered()
{
    storeClipboard(9);
}

void SuperClipboard::on_actionRetrieveSlot_1_triggered()
{
    retrieveClipboard(1);
}

void SuperClipboard::on_actionRetrieveSlot_2_triggered()
{
    retrieveClipboard(2);
}

void SuperClipboard::on_actionRetrieveSlot_3_triggered()
{
    retrieveClipboard(3);
}

void SuperClipboard::on_actionRetrieveSlot_4_triggered()
{
    retrieveClipboard(4);
}

void SuperClipboard::on_actionRetrieveSlot_5_triggered()
{
    retrieveClipboard(5);
}

void SuperClipboard::on_actionRetrieveSlot_6_triggered()
{
    retrieveClipboard(6);
}

void SuperClipboard::on_actionRetrieveSlot_7_triggered()
{
    retrieveClipboard(7);
}

void SuperClipboard::on_actionRetrieveSlot_8_triggered()
{
    retrieveClipboard(8);
}

void SuperClipboard::on_actionRetrieveSlot_9_triggered()
{
    retrieveClipboard(9);
}

void SuperClipboard::on_actionRetrieveSelectedSlot_triggered()
{
    QList<QTableWidgetItem*> list = ui->tableWidgetSlots->selectedItems();
    if (!list.isEmpty() && ui->tableWidgetSlots->hasFocus()) {
        int row = ui->tableWidgetSlots->row(list.first());
        retrieveClipboard(row + 1);
    } else {
        list = ui->tableWidgetHistory->selectedItems();
        if (!list.isEmpty() && ui->tableWidgetHistory->hasFocus()) {
            int row = ui->tableWidgetHistory->row(list.first());

            QString label = ui->tableWidgetHistory->item(row, 1)->text() + ui->tableWidgetHistory->item(row, 0)->text();
            label.replace("-", "").replace(" ", "").replace(":", "").replace(":", "").replace(".", "");
            retrieveClipboardHistory(label);
        }
    }
}

void SuperClipboard::on_actionOpenSelectedSlot_triggered()
{
    QList<QTableWidgetItem*> list = ui->tableWidgetSlots->selectedItems();
    if (!list.isEmpty() && ui->tableWidgetSlots->hasFocus()) {
        int row = ui->tableWidgetSlots->row(list.first());
        openClipboard(row + 1);
    } else {
        list = ui->tableWidgetHistory->selectedItems();
        if (!list.isEmpty() && ui->tableWidgetHistory->hasFocus()) {
            int row = ui->tableWidgetHistory->row(list.first());

            QString label = ui->tableWidgetHistory->item(row, 1)->text() + ui->tableWidgetHistory->item(row, 0)->text();
            label.replace("-", "").replace(" ", "").replace(":", "").replace(":", "").replace(".", "");
            openClipboardHistory(label);
        }
    }
}

void SuperClipboard::on_actionFollowSelectedSlot_triggered()
{
    QList<QTableWidgetItem*> list = ui->tableWidgetSlots->selectedItems();
    if (!list.isEmpty() && ui->tableWidgetSlots->hasFocus()) {
        int row = ui->tableWidgetSlots->row(list.first());
        followClipboard(row + 1);
    } else {
        list = ui->tableWidgetHistory->selectedItems();
        if (!list.isEmpty() && ui->tableWidgetHistory->hasFocus()) {
            int row = ui->tableWidgetHistory->row(list.first());

            QString label = ui->tableWidgetHistory->item(row, 1)->text() + ui->tableWidgetHistory->item(row, 0)->text();
            label.replace("-", "").replace(" ", "").replace(":", "").replace(":", "").replace(".", "");
            followClipboardHistory(label);
        }
    }
}

void SuperClipboard::on_actionClearSelectedSlot_triggered()
{
    QList<QTableWidgetItem*> list = ui->tableWidgetSlots->selectedItems();
    if (!list.isEmpty() && ui->tableWidgetSlots->hasFocus()) {
        int row = ui->tableWidgetSlots->row(list.first());
        clearClipboard(row + 1);
    } else {
        list = ui->tableWidgetHistory->selectedItems();
        if (!list.isEmpty() && ui->tableWidgetHistory->hasFocus()) {
            int row = ui->tableWidgetHistory->row(list.first());

            QString label = ui->tableWidgetHistory->item(row, 1)->text() + ui->tableWidgetHistory->item(row, 0)->text();
            label.replace("-", "").replace(" ", "").replace(":", "").replace(":", "").replace(".", "");
            clearClipboardHistory(label);
        }
    }
}

void SuperClipboard::on_actionStoreSelectedSlot_triggered()
{
    QList<QTableWidgetItem*> list = ui->tableWidgetSlots->selectedItems();
    if (!list.isEmpty() && ui->tableWidgetSlots->hasFocus()) {
        int row = ui->tableWidgetSlots->row(list.first());
        storeClipboard(row + 1);
    }
}

void SuperClipboard::keyPressEvent(QKeyEvent *event) {
    if (event->modifiers() & Qt::ControlModifier) {
        if (event->key() == Qt::Key_C) {
            on_actionRetrieveSelectedSlot_triggered();
        } else if (event->key() == Qt::Key_X) {
            on_actionRetrieveSelectedSlot_triggered();
            on_actionClearSelectedSlot_triggered();
        } else if (event->key() == Qt::Key_V) {
            on_actionStoreSelectedSlot_triggered();
        }
    }
}

void SuperClipboard::on_tabWidget_currentChanged(int index)
{
    if (index == 0) {
        on_tableWidgetSlots_itemSelectionChanged();
    } else {
        on_tableWidgetHistory_itemSelectionChanged();
    }
}
