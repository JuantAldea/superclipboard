#ifndef SYSTEMKEYBOARDX11_H
#define SYSTEMKEYBOARDX11_H

#include <QThread>
#include <X11/Xlib.h>

// X11 defines bool and messes with QT types
#undef Bool

class SystemKeyboardX11 : public QThread
{
    Q_OBJECT

public:
    explicit SystemKeyboardX11(QObject *parent = 0);
    ~SystemKeyboardX11();

protected:
    void run();

signals:
    void keyPressed(int index, bool store);

private:
    Display *display;
    Window root;

};

#endif // SYSTEMKEYBOARDX11_H
