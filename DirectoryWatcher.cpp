#include "DirectoryWatcher.h"
#include <QFile>
#include <QDir>
#include <QDateTime>
#include <QDebug>

DirectoryWatcher::DirectoryWatcher(const QString &path, QObject *parent):
    fileSystemWatcher(QStringList(path), parent)
{
    entryList = QDir(path).entryList();
    foreach (const QString &entry, entryList) {
        if (entry != "." && entry != "..") {
            fileSystemWatcher.addPath(path + entry);
        }
    }

    connect(&fileSystemWatcher, SIGNAL(directoryChanged(QString)), this, SLOT(onDirectoryChanged(QString)));
    connect(&fileSystemWatcher, SIGNAL(fileChanged(QString)), this, SLOT(onFileChanged(QString)));
}

void DirectoryWatcher::onDirectoryChanged(const QString &path)
{
    QStringList changedList = QDir(path).entryList();

    foreach (const QString &entry, entryList) {
        if (entry != "." && entry != "..") {
            if (!changedList.contains(entry)) {
                emit fileRemoved(path + entry);
                fileSystemWatcher.removePath(path + entry);
            }
        }
    }

    foreach (const QString &entry, changedList) {
        if (entry != "." && entry != "..") {
            if (!entryList.contains(entry)) {
                emit fileAdded(path + entry);
                fileSystemWatcher.addPath(path + entry);
            }
        }
    }

    entryList = changedList;
}

void DirectoryWatcher::onFileChanged(const QString &path)
{
    if (QFile::exists(path)) {
        emit fileChanged(path);
    }
}

