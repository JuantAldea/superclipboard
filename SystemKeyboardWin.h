#ifndef SYSTEMKEYBOARDWIN_H
#define SYSTEMKEYBOARDWIN_H

#pragma once
#include <QObject>
#include <Windows.h>
#include "SuperClipboard.h"

class SystemKeyboardWin : public QObject
{

Q_OBJECT
public:
    static SystemKeyboardWin * instance();
    ~SystemKeyboardWin();
    void setSuperClipboard(SuperClipboard *superClipboard);

private:
    SuperClipboard *superClipboard;

    static SystemKeyboardWin *uniqueInstance;
    SystemKeyboardWin();

    static LRESULT CALLBACK keyboardProcedure(int nCode, WPARAM wParam, LPARAM lParam);
    void keyboardProcedure(WPARAM wParam, LPARAM lParam);

    HHOOK keyboardHook;
    bool shiftPressed;
    bool controlPressed;
    int numberPressed;
    int fNumberPressed;
};

#endif // SYSTEMKEYBOARDREADWRITE_H
