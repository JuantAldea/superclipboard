#ifndef SUPERCLIPBOARD_H
#define SUPERCLIPBOARD_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include <QMimeData>
#include "DirectoryWatcher.h"
#include "ClipboardManager.h"

#ifndef WIN32
class SystemKeyboardX11;
#endif

#define SLOTS_PATH QString("slots/")
#define HISTORY_PATH QString("slots_history/")
#define VERSION QString("1.3")

namespace Ui {
class SuperClipboard;
}

class SuperClipboard : public QMainWindow
{
    Q_OBJECT

public:
    explicit SuperClipboard(QWidget *parent = 0);
    ~SuperClipboard();
    void storeClipboard(int index);
    void retrieveClipboard(int index);
    void openClipboard(int index);
    void followClipboard(int index);
    void clearClipboard(int index);

    void storeClipboardHistory(int index);
    void retrieveClipboardHistory(const QString &label);
    void followClipboardHistory(const QString &label);
    void clearClipboardHistory(const QString &label);
    void openClipboardHistory(const QString &label);

private slots:
    void systemTrayIcon_activated(QSystemTrayIcon::ActivationReason reason);
    void watcher_slotTouched(const QString &fileName);
    void watcherHistory_slotTouched(const QString &fileName);
    void on_actionQuit_triggered();
    void on_actionAbout_SuperClipboard_triggered();
    void on_actionStoreSlot_1_triggered();
    void on_actionStoreSlot_2_triggered();
    void on_actionStoreSlot_3_triggered();
    void on_actionStoreSlot_4_triggered();
    void on_actionStoreSlot_5_triggered();
    void on_actionStoreSlot_6_triggered();
    void on_actionStoreSlot_7_triggered();
    void on_actionStoreSlot_8_triggered();
    void on_actionStoreSlot_9_triggered();
    void on_actionRetrieveSlot_1_triggered();
    void on_actionRetrieveSlot_2_triggered();
    void on_actionRetrieveSlot_3_triggered();
    void on_actionRetrieveSlot_4_triggered();
    void on_actionRetrieveSlot_5_triggered();
    void on_actionRetrieveSlot_6_triggered();
    void on_actionRetrieveSlot_7_triggered();
    void on_actionRetrieveSlot_8_triggered();
    void on_actionRetrieveSlot_9_triggered();
    void on_actionShortcuts_triggered();
    void on_tableWidgetSlots_cellDoubleClicked(int row, int column);
    void on_tableWidgetSlots_customContextMenuRequested(const QPoint &pos);
    void on_tableWidgetSlots_cellChanged(int row, int column);
    void on_tableWidgetSlots_itemSelectionChanged();
    void on_tableWidgetHistory_cellDoubleClicked(int row, int column);
    void on_tableWidgetHistory_customContextMenuRequested(const QPoint &pos);
    void on_tableWidgetHistory_cellChanged(int row, int column);
    void on_tableWidgetHistory_itemSelectionChanged();
    void on_actionRetrieveSelectedSlot_triggered();
    void on_actionOpenSelectedSlot_triggered();
    void on_actionFollowSelectedSlot_triggered();
    void on_actionClearSelectedSlot_triggered();
    void on_actionStoreSelectedSlot_triggered();
    void on_tabWidget_currentChanged(int index);

private:
    Ui::SuperClipboard *ui;
    QSystemTrayIcon systemTrayIcon;
    DirectoryWatcher watcher;
    DirectoryWatcher watcherHistory;
    void closeEvent(QCloseEvent *event);
    ClipboardManager clipboardManager;
    void refreshSlot(int index);
    void refreshSlotHistory(const QString &label);
    void keyPressEvent(QKeyEvent *event);

#ifndef WIN32
    SystemKeyboardX11 *systemKeyboardX11;

private slots:
    void keyPressed(int index, bool store);
#endif

};

#endif // SUPERCLIPBOARD_H
