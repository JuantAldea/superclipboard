#include "ClipboardManager.h"

#include <QMimeData>
#include <QFile>
#include <QStringList>
#include <QIODevice>
#include <QDate>

QDateTime ClipboardManager::fromMSecsSinceEpoch(const quint64 &mSecsSinceEpoch) {
    int mSecs = mSecsSinceEpoch % 1000;
    QDateTime dateTime;
    dateTime.setTime_t(mSecsSinceEpoch / 1000);
    return dateTime.addMSecs(mSecs);
}

quint64 ClipboardManager::toMSecsSinceEpoch(const QDateTime &dateTime) {
    quint64 mSecsSinceEpoch = dateTime.toTime_t();
    mSecsSinceEpoch *= 1000;
    mSecsSinceEpoch += dateTime.time().msec();
    return mSecsSinceEpoch;
}

bool ClipboardManager::write(const QString &filePath, const QMimeData &mimeData)
{
    bool result = false;
    QFile file(filePath);
    if (file.open(QIODevice::WriteOnly)) {
        const QStringList &formats = mimeData.formats();
        foreach (const QString &i, formats) {
            const QByteArray &format = i.toUtf8();
            int formatSize = format.size();
            file.write((const char*)&formatSize, sizeof(int));
            file.write(format, formatSize);

            const QByteArray &data = mimeData.data(i);
            int dataSize = data.size();
            file.write((const char*)&dataSize, sizeof(int));
            file.write(data, dataSize);
        }
        file.close();
        result = true;
    }
    return result;
}

bool ClipboardManager::write(const QString &filePath, const QString &text, const QDateTime &dateTime, const QString &tag)
{
    bool result = false;
    QFile file(filePath);
    if (file.open(QIODevice::WriteOnly)) {
        // Write text
        const QByteArray &textb = text.toUtf8();
        int textSize = textb.size();
        file.write((const char*)&textSize, sizeof(int));
        file.write(textb, textSize);

        // Write date and time
        quint64 mSecs = toMSecsSinceEpoch(dateTime);
        file.write((const char*)&mSecs, sizeof(quint64));

        // Write tag
        const QByteArray &tagb = tag.toUtf8();
        int tagSize = tagb.size();
        file.write((const char*)&tagSize, sizeof(int));
        file.write(tagb);

        file.close();
        result = true;
    }
    return result;
}

bool ClipboardManager::read(const QString &filePath, QMimeData &mimeData)
{
    bool result = false;
    QFile file(filePath);
    if (file.open(QIODevice::ReadOnly)) {
        while (!file.atEnd()) {
            int formatSize;
            file.read((char*)&formatSize, sizeof(int));
            QString format(file.read(formatSize));

            int dataSize;
            file.read((char*)&dataSize, sizeof(int));
            QByteArray data(file.read(dataSize));

            mimeData.setData(format, data);
        }
        file.close();
        result = true;
    }
    return result;
}

bool ClipboardManager::read(const QString &filePath, QString &text, QDateTime &dateTime, QString &tag)
{
    bool result = false;
    QFile file(filePath);
    if (file.open(QIODevice::ReadOnly)) {
        // Read tag
        int textSize;
        file.read((char*)&textSize, sizeof(int));
        text.append(file.read(textSize));

        // Read date and time
        quint64 mSecs;
        file.read((char*)&mSecs, sizeof(quint64));
        QDateTime dateTimeAux = fromMSecsSinceEpoch(mSecs);
        dateTime.setDate(dateTimeAux.date());
        dateTime.setTime(dateTimeAux.time());

        // Read tag
        int tagSize;
        file.read((char*)&tagSize, sizeof(int));
        tag.append(file.read(tagSize));

        file.close();
        result = true;
    }
    return result;
}
