#include "SuperClipboard.h"
#include <QApplication>
#include <QDir>
#include <QTranslator>
#include <QLibraryInfo>
#include <QLocale>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString localeName = QLocale::system().name();
    QTranslator qtTranslator;
    qtTranslator.load("qt_" + localeName, QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    a.installTranslator(&qtTranslator);
    QTranslator superclipboardTranslator;
    superclipboardTranslator.load("superclipboard_" + localeName, ":/");
    a.installTranslator(&superclipboardTranslator);

    if (!QDir(SLOTS_PATH).exists()) {
        QDir().mkdir(SLOTS_PATH);
    }

    if (!QDir(HISTORY_PATH).exists()) {
        QDir().mkdir(HISTORY_PATH);
    }

    SuperClipboard w;
    w.setWindowState(Qt::WindowMaximized);

    return a.exec();
}
