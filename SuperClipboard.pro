#-------------------------------------------------
#
# Project created by QtCreator 2012-12-07T21:40:00
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SuperClipboard
TEMPLATE = app


SOURCES += main.cpp\
        ClipboardManager.cpp \
        SuperClipboard.cpp \
        DirectoryWatcher.cpp

HEADERS  += ClipboardManager.h \
        SuperClipboard.h \
        DirectoryWatcher.h

win32 {
    SOURCES += SystemKeyboardWin.cpp
    HEADERS += SystemKeyboardWin.h
}

unix {
    SOURCES += SystemKeyboardX11.cpp
    HEADERS += SystemKeyboardX11.h
    LIBS += -lX11
}

FORMS    += \
    SuperClipboard.ui

RESOURCES += \
    resources.qrc

RC_FILE = exeicon.rc

TRANSLATIONS = superclipboard_es.ts
