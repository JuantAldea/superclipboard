#ifndef CLIPBOARDMANAGER_H
#define CLIPBOARDMANAGER_H

#include <QObject>
#include <QMimeData>

class ClipboardManager : public QObject
{
    Q_OBJECT

public:
    static QDateTime fromMSecsSinceEpoch(const quint64 &mSecsSinceEpoch);
    static quint64 toMSecsSinceEpoch(const QDateTime &dateTime);

    bool write(const QString &filePath, const QMimeData &mimeData);
    bool write(const QString &filePath, const QString &text, const QDateTime &dateTime, const QString &tag);

    bool read(const QString &filePath, QMimeData &mimeData);
    bool read(const QString &filePath, QString &text, QDateTime &dateTime, QString &tag);
};

#endif // CLIPBOARDMANAGER_H
