#include "SystemKeyboardWin.h"

#define KEY_CONTROL 162
#define KEY_SHIFT 160
#define KEY_1 49
#define KEY_9 57
#define KEY_F1 112
#define KEY_F9 120

SystemKeyboardWin* SystemKeyboardWin::uniqueInstance = 0;

SystemKeyboardWin* SystemKeyboardWin::instance() {
    if (uniqueInstance == NULL) {
        uniqueInstance = new SystemKeyboardWin();
    }
    return uniqueInstance;
}

SystemKeyboardWin::SystemKeyboardWin() :
    QObject()
{
    keyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL, keyboardProcedure, GetModuleHandle(NULL), 0);
    shiftPressed = false;
    controlPressed = false;
    numberPressed = -1;
    fNumberPressed = -1;
}

SystemKeyboardWin::~SystemKeyboardWin()
{
    uniqueInstance = NULL;
}

void SystemKeyboardWin::setSuperClipboard(SuperClipboard *superClipboard) {
    this->superClipboard = superClipboard;
}

LRESULT CALLBACK SystemKeyboardWin::keyboardProcedure(int nCode, WPARAM wParam, LPARAM lParam)
{
    if (nCode == HC_ACTION) {
        instance()->keyboardProcedure(wParam, lParam);
    }
    return false;
}

void SystemKeyboardWin::keyboardProcedure(WPARAM wParam, LPARAM lParam) {
    if (wParam == WM_KEYDOWN) {
        KBDLLHOOKSTRUCT *pKeyboard = (KBDLLHOOKSTRUCT*)lParam;

        bool stateChanged = false;
        if (pKeyboard->vkCode == KEY_CONTROL && !controlPressed) {
            controlPressed = true;
            stateChanged = true;
        } else if (pKeyboard->vkCode == KEY_SHIFT && !shiftPressed) {
            shiftPressed = true;
            stateChanged = true;
        } else if (KEY_1 <= pKeyboard->vkCode && pKeyboard->vkCode <= KEY_9 && numberPressed == -1) {
            numberPressed = pKeyboard->vkCode - KEY_1 + 1;
            stateChanged = true;
        } else if (KEY_F1 <= pKeyboard->vkCode && pKeyboard->vkCode <= KEY_F9 && fNumberPressed == -1) {
            fNumberPressed = pKeyboard->vkCode - KEY_F1 + 1;
            stateChanged = true;
        }

        if (stateChanged) {
            if (controlPressed && shiftPressed) {
                if (numberPressed > 0) {
                    superClipboard->storeClipboard(numberPressed);
                } else if (fNumberPressed > 0) {
                    superClipboard->retrieveClipboard(fNumberPressed);
                }
            }
        }
    } else if (wParam == WM_KEYUP) {
        KBDLLHOOKSTRUCT *pKeyboard = (KBDLLHOOKSTRUCT*)lParam;

        if (pKeyboard->vkCode == KEY_CONTROL) {
            controlPressed = false;
        } else if (pKeyboard->vkCode == KEY_SHIFT) {
            shiftPressed = false;
        } else if (KEY_1 <= pKeyboard->vkCode && pKeyboard->vkCode <= KEY_9) {
            numberPressed = -1;
        } else if (KEY_F1 <= pKeyboard->vkCode && pKeyboard->vkCode <= KEY_F9) {
            fNumberPressed = -1;
        }
    }
}

