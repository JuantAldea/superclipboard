#ifndef DIRECTORYWATCHER_H
#define DIRECTORYWATCHER_H

#include <QObject>
#include <QFileSystemWatcher>
#include <QStringList>

class DirectoryWatcher : public QObject
{
    Q_OBJECT

public:
    DirectoryWatcher(const QString &path, QObject *parent = NULL);

private:
    QStringList entryList;
    QFileSystemWatcher fileSystemWatcher;

signals:
    void fileAdded(const QString &path);
    void fileRemoved(const QString &path);
    void fileChanged(const QString &path);

private slots:
    void onDirectoryChanged(const QString &path);
    void onFileChanged(const QString &path);

};

#endif // DIRECTORYWATCHER_H
