#include "SystemKeyboardX11.h"

SystemKeyboardX11::SystemKeyboardX11(QObject *parent) :
    QThread(parent)
{
    display = XOpenDisplay(NULL);
    root = DefaultRootWindow(display);

    for (int i = 0; i < 9; i++) {
        for (int j = 255; j >= 0; j--) {
            if ((j & (ShiftMask | ControlMask)) == (ShiftMask | ControlMask)) {
                XGrabKey(display, 10 + i, ShiftMask | ControlMask | j, root, False, GrabModeAsync, GrabModeAsync);
                XGrabKey(display, 67 + i, ShiftMask | ControlMask | j, root, False, GrabModeAsync, GrabModeAsync);
            }
        }
    }
}

void SystemKeyboardX11::run()
{
    while(1) {
        XEvent ev;
        XNextEvent(display, &ev);
        if (ev.type == KeyPress) {
            unsigned int keycode = ((XKeyPressedEvent*)&ev)->keycode;

            if (10 <= keycode && keycode <= 18) {
                emit keyPressed(keycode - 9, true);
            } else if (67 <= keycode && keycode <= 75) {
                emit keyPressed(keycode - 66, false);
            }
        }
    }
}

SystemKeyboardX11::~SystemKeyboardX11()
{
    for (int i = 0; i < 9; i++) {
        for (int j = 255; j >= 0; j--) {
            if ((j & (ShiftMask | ControlMask)) == (ShiftMask | ControlMask)) {
                XUngrabKey(display, 10 + i, ShiftMask | ControlMask | j, root);
                XUngrabKey(display, 67 + i, ShiftMask | ControlMask | j, root);
            }
        }
    }
    //XCloseDisplay(display); //this function used to be non-blocking...
}
